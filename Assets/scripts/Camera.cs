﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Camera : MonoBehaviour
{

    public Transform target;        
    public float sensitivity = 50f;
    public float distance=10f;
    Vector3 offset;
    Vector3 pitchaxis;   
    Ray cameracollusioncheck;
    float pitch;
    
    // Start is called before the first frame update
    void Start()
    {
        pitchaxis = target.right;
        transform.localPosition = target.position + -target.forward * (distance);    
        offset = -target.position + transform.position;
        
        pitchangle();           
    }
    
    
    void FixedUpdate()
    {        
        Movement();
        CameraCollusion();        
        pitchangle();        
    }
    void Movement() {        
        Cursor.lockState = CursorLockMode.Locked;
        float xdisplacement = Input.GetAxis("Mouse X");
        float ydisplacement = Input.GetAxis("Mouse Y");        

        float rotX = xdisplacement * sensitivity * Time.deltaTime;
        float rotY = ydisplacement * sensitivity * Time.deltaTime;        
        if (Mathf.Abs(rotY + pitch) >= 85) {
            if (pitch < 0) {
                rotY = -85 - pitch;
            }else
            {
                rotY = 85-pitch;
            }
        }
               
        var temp = Quaternion.AngleAxis(rotX,Vector3.up) * offset;
        pitchaxis = Quaternion.AngleAxis(rotX, Vector3.up) * pitchaxis;
        temp = Quaternion.AngleAxis(-rotY, pitchaxis) * temp;
        
        offset = temp;

        Vector3 Newpos = target.position + offset;

        transform.position = Newpos;
        transform.LookAt(target);        
    }

    void CameraCollusion() {
        cameracollusioncheck = new Ray(target.position,offset);
        RaycastHit h;
        Debug.DrawRay(target.position,offset.normalized,Color.red);
        if(Physics.Raycast(cameracollusioncheck,out h)){
            //thing.transform.position = h.point;
            //Debug.Log(h.collider.gameObject.transform.name);
            //var temp = offset.normalized * (h.distance - 2f);
            if (h.distance < distance) transform.position = h.point + new Vector3(0f, 0.2f, 0f);
        }
    }

    void pitchangle() {
        if (Vector3.Angle(offset, Vector3.up) <= 90)
        {
            pitch = -Vector3.Angle(Vector3.Cross(pitchaxis, -Vector3.up), offset);
        }
        else {
            pitch = Vector3.Angle(Vector3.Cross(pitchaxis, -Vector3.up), offset);
        }
    }
    }

