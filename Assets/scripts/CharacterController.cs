﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CharacterController : MonoBehaviour
{
    public float speed =10f;
    public Transform main;
    public bool playable=true;
    public Transform target;
    Vector3 velocity;
    public bool onground;
    public float normalangle;
    Ray groundcheckray;
    RaycastHit groundcheckrayhit;
    Animator anim;
    Rigidbody rigid;
    Transform transform;
    
    // Start is called before the first frame update
    void Start()
    {
        anim = GetComponent<Animator>();
        rigid = GetComponent<Rigidbody>();
        transform = GetComponent<Transform>();       
        main = FindObjectOfType<Camera>().GetComponent<Transform>();
    }

    
    void FixedUpdate()
    {
        groundCheck();       
        Movement();        
    }

    void Movement() {

        if (playable)
        {
            velocity = new Vector3();
            if (Input.GetAxis("Vertical") != 0)
            {
                var temp = main.forward;
                temp.y = 0;
                velocity = (temp * Input.GetAxis("Vertical"));
                anim.SetBool("Isrunning", true);
            }

            if (Input.GetAxis("Horizontal") != 0)
            {
                velocity = velocity + (main.right * Input.GetAxis("Horizontal"));
                anim.SetBool("Isrunning", true);
            }
            velocity = velocity.normalized * (speed);          

        }
        else {
            Ai();
        }

        

        if (onground)
        {
            if (normalangle <= 60)
            {
                velocity = Quaternion.FromToRotation(transform.up, groundcheckrayhit.normal) * velocity;  
                              
                if (velocity.magnitude == 0)
                {                    
                        rigid.velocity = Vector3.Lerp(rigid.velocity, velocity, 0.5f);                    
                }
                else rigid.velocity = velocity;
            }
        }
        else {
            var temp = velocity;
            temp.y = 0f;          
            rigid.velocity = rigid.velocity + 0.01f * temp;
        }
        

        if (rigid.velocity.magnitude >= 0.7f) {
            anim.SetBool("Isrunning", true);           
            if (Vector3.Dot(rigid.velocity, transform.forward) <= 0) anim.SetBool("Isrunning", false);            
            else if (Vector3.Dot(rigid.velocity, transform.forward) > 0) anim.SetBool("Isrunning", true);           

        }
        else if (rigid.velocity.magnitude < 0.7f) anim.SetBool("Isrunning", false);
        var velo = velocity;       
        if (velo.magnitude != 0)
        {            
            Quaternion rot = new Quaternion();
            velo = Vector3.ProjectOnPlane (velo,Vector3.up);
            rot.SetLookRotation(velo.normalized, Vector3.up);            
            transform.rotation = Quaternion.Slerp(transform.rotation, rot, 20f*Time.deltaTime);
        }
    }
    void Ai()
    {
        if (!playable)
        {
            if (Vector3.Distance(target.position, transform.position) > 15f)
            {
                var temp = target.position - transform.position;
                temp.Normalize();
                temp.y = 0;
                var temp2 = rigid.velocity.y;
                velocity = temp * speed;                               
            }            
            else{                
                var temp = new Vector3();                
                velocity = temp;                            
            }            
        }
    }
    void groundCheck() {
        groundcheckray = new Ray(transform.position + new Vector3(0f, 0.1f, 0f), Vector3.down);
        Physics.Raycast(groundcheckray, out groundcheckrayhit);
        if(groundcheckrayhit.distance<=0.5)
        {
            onground = true;
            normalangle = Vector3.Angle(Vector3.up, groundcheckrayhit.normal);
        }
        else onground = false;
    }
}

