﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class IdleBehaviour : StateMachineBehaviour
{
    float time;
    float timedef;


    public override void OnStateEnter(Animator animator, AnimatorStateInfo animatorStateInfo, int layerIndex)
    {        
        
        time = Time.fixedTime;
        

        if (!animator.GetBool("Isrunning"))
        {
            animator.SetInteger("idleRN", 0);
        }
        else {
            animator.SetInteger("idleRN", 2);
        }

              
    }
    public override void OnStateUpdate(Animator animator, AnimatorStateInfo animatorStateInfo, int layerIndex)
    {
        if ((animatorStateInfo.normalizedTime % animatorStateInfo.length / animatorStateInfo.length) < 0.1f) {
            IdleThingy(animator);
        }
    }

    void IdleThingy(Animator animator) {
        float rnd = Random.Range(0.7f, 3.0f);
        timedef = Time.fixedTime - time;

        if (timedef >= 20 * rnd)
        {
            time = Time.fixedTime;
            var temp = Random.Range(1, 3);
            animator.SetInteger("idleRN", temp);
        }
    }

}
